﻿using System.Collections.Generic;

namespace MyBack.Models
{
    public class WorkInfoView
    {
        public List<WorkInfo> WorkInfo { get; set; }
        public int worksCount { get; set; }
    }
}
