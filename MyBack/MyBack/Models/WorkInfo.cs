﻿namespace MyBack.Models
{
    public class WorkInfo
    {
        public int Id { get; set; }
        public string WorkName { get; set; }
        //In fe iml. like "old"
        public int PersonCount { get; set; } 
        public int AvarageSalary { get; set; }
    }
}
