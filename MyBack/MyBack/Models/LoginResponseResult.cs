﻿namespace MyBack.Models
{
    public class LoginResponseResult
    {
        public string Login { get; set; }
        public string Message { get; set; }

    }
}
