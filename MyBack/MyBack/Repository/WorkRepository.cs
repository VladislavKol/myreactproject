﻿using Dapper;
using MyBack.Interfaces;
using MyBack.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MyBack.Repository
{
    public class WorkRepository : IWorkRepository
    {
        private const string connectionString = "Server=localhost;Initial Catalog = test; Integrated Security = True";

        public async Task<WorkInfo> getWorkById(int id)
        {
            // threadsleep for slow internet connection effect
            Thread.Sleep(400);
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var member = (await db.QueryAsync<WorkInfo>($"SELECT * FROM Myreact where id={id}")).First();
                return member;
            }
        }

        public async Task<WorkInfoView> getPage(int page, int pageSize)
        {
            // threadsleep for slow internet connection effect
            Thread.Sleep(400);
            var list = new List<WorkInfo>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var a = (await db.QueryAsync<int>($"SELECT count(*) FROM Myreact ")).First();
                list = (await db.QueryAsync<WorkInfo>($"SELECT * FROM Myreact order by WorkName OFFSET {(page - 1) * pageSize} ROWS FETCH NEXT {pageSize} ROWS ONLY")).ToList();
                return new WorkInfoView() { WorkInfo = list, worksCount = a };
            }
        }

        public async Task addUser(string workName, int personCount, int avarageSalary)
        {
            // threadsleep for slow internet connection effect
            Thread.Sleep(400);
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                string insertQuery = @"INSERT INTO [dbo].[Myreact]([WorkName], [PersonCount], [AvarageSalary]) VALUES (@WorkName, @PersonCount, @AvarageSalary)";

                await db.ExecuteAsync(insertQuery, new
                {
                    workName,
                    personCount,
                    avarageSalary
                });
            }
        }

        public async Task deleteUser(int id)
        {
            // threadsleep for slow internet connection effect
            Thread.Sleep(400);
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                string insertQuery = @"DELETE FROM Myreact WHERE id = @id";

                await db.ExecuteAsync(insertQuery, new { id });
            }
        }
    }
}
