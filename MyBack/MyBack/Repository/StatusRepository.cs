﻿using Dapper;
using MyBack.Interfaces;
using MyBack.Models;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyBack.Repository
{
    public class StatusRepository : IStatusRepository
    {
        private const string connectionString = "Server=localhost;Initial Catalog = test; Integrated Security = True";

        public async Task<string> getStatus()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var member = (await db.QueryAsync<StatusViewModel>($"SELECT * FROM Mystatus where id=2")).ToList().First();
                return member.Stattus;
            }
        }

        public async Task updateStatus(string status) 
        {
            using (IDbConnection db = new SqlConnection(connectionString)) 
            {
                string insertQuery = @"Update Mystatus set stattus = @status WHERE id = 2";
                await db.ExecuteAsync(insertQuery, new { status });
            }

        }
    }
}
