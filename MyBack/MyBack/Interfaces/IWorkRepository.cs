﻿using MyBack.Models;
using System.Threading.Tasks;

namespace MyBack.Interfaces
{
    public interface IWorkRepository
    {
        Task<WorkInfo> getWorkById(int id);

        Task<WorkInfoView> getPage(int page, int pageSize);

        Task addUser(string workName, int personCount, int avarageSalary);

        Task deleteUser(int id);
    }
}
