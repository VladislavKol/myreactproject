﻿using System.Threading.Tasks;

namespace MyBack.Interfaces
{
    public interface IStatusRepository
    {
        Task<string> getStatus();
        Task updateStatus(string status);
    }
}
