﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBack.Interfaces;
using MyBack.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyBack.Controllers
{
    [ApiController]
    [Route("api/worker")]
    public class DataBaserController : Controller
    {
        private IWorkRepository workRepository;
        private static Dictionary<string, string> users = new Dictionary<string, string>() { ["admin"] = "admin", ["Vladislav"] = "koltyshev" };

        public DataBaserController(IWorkRepository workRepository)
        {
            this.workRepository = workRepository;
        }

        [Authorize]
        [HttpGet("/name")]
        public async Task<WorkInfo> getWorkById(int id)
        {
            return await workRepository.getWorkById(id);
        }

        [Authorize]
        [HttpGet("/work")]
        public async Task<WorkInfoView> getPage(int page, int pageSize)
        {
            return await workRepository.getPage(page, pageSize);
        }


        [Authorize]
        [HttpPost("/adduser")]
        public async Task<IActionResult> addUser([FromBody] WorkInfo model)
        {
            await workRepository.addUser(model.WorkName, model.PersonCount, model.AvarageSalary);
            return Ok();
        }

        [Authorize]
        [HttpDelete("/deleteUser")]
        public async Task<IActionResult> deleteUser(int id)
        {
            await workRepository.deleteUser(id);
            return Ok();
        }
    }
}
