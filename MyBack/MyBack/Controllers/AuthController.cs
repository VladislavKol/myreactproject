﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBack.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MyBack.Controllers
{
    [ApiController]
    [Route("api/worker")]
    public class AuthController : Controller
    {
        private readonly static Dictionary<string, string> users = new Dictionary<string, string>() {["vladislav"] = "koltyshev"};

        [HttpGet("/authme")]
        public bool isAutorized()
        {
            return User.Identity.IsAuthenticated;
        }

        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            string message = null;
            if (User.Identity.IsAuthenticated)
            {
                message = "User is alredy authorized";
                return Ok(new LoginResponseResult { Message = message, Login = model.Login });
            }
            if (!users.ContainsKey(model.Login))
            {
                message = "Incorrect login";
                return Ok(new LoginResponseResult { Message = message, Login = model.Login });
            }
            if (users[model.Login] != model.Password) 
            {
                message = "Incorrect password";
                return Ok(new LoginResponseResult { Message = message, Login = model.Login });
            }
           
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name,model.Login)
            };
            var claimIdentity = new ClaimsIdentity(claims, "Cookie");
            var claimPrincipal = new ClaimsPrincipal(claimIdentity);
            await HttpContext.SignInAsync("Cookie", claimPrincipal);
            return Ok(new LoginResponseResult { Message = null, Login = model.Login });
        }

        [Authorize]
        [HttpPost("/logout")]
        public async Task<IActionResult> logout()
        {
            await HttpContext.SignOutAsync("Cookie");
            return Ok();
        }


    }
}
