﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyBack.Interfaces;
using MyBack.Models;
using System.Threading.Tasks;

namespace MyBack.Controllers
{
    [ApiController]
    [Route("api/status")]
    public class StatusController : Controller
    {
        private IStatusRepository statusRepository;
        public StatusController(IStatusRepository statusRepository)
        {
            this.statusRepository = statusRepository;
        }


        [Authorize]
        [HttpGet("/current")]
        public async Task<string> getStatus() 
        {
            return await statusRepository.getStatus();
        } 


        [HttpPost("/update")]
        public async Task updateStatus([FromBody]StatusViewModel status) 
        {
            await statusRepository.updateStatus(status.Stattus);
        }
    }
}
