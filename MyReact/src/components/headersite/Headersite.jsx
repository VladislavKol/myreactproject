import React from 'react';
import h from './Headersite.module.css';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

const Headersite = (props) => {
    return (
        <header className={h.siteheader}>
           {!props.isAutorized? <NavLink to = '/login'><p>Login</p></NavLink>: <NavLink className={h.isEntered} to = '/login'><p>{props.login || "admin"}</p></NavLink>}
        </header>
    );
  }

  let mapStateToProps =(state)=>
  {
      return{
        isAutorized: state.auth.isAutorized,
        login: state.auth.login
      }
  } 

  export default connect(mapStateToProps)(Headersite);

