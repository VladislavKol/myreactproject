import React from 'react';
import s from './App.module.css';
import Navigation from '../navigation/Navigation.jsx';
import Headersite from '../headersite/Headersite.jsx';
import Footersite from '../footersite/Footersite.jsx';
import { BrowserRouter, Route } from "react-router-dom";
import ProfileContainer from '../main/Profiles//ProfileContainer.jsx';
import { PATH } from '../../constants/constants';
import DataBaserContainer from '../main/Databaser/DataBaserContainer';
import DataInfoContainer from '../main/DataInfo/DataInfoContainer';
import LoginContainer from '../main/Auth/LoginContainer';
import DataEditorContainer  from '../main/DataEditor/DataEditorContainer';


class App extends React.Component{
  render()
  {
    return ( 
      <BrowserRouter>
        <div className={s.wrapper}>
          <Headersite />
          <Navigation />
          <main className={s.maincontent}>
            <Route path={PATH.PROFILE} render={() => <ProfileContainer />} />
            <Route path={PATH.DATABASER} render={() => <DataBaserContainer />} />
            <Route path={PATH.DATAINFO} render={() => <DataInfoContainer />} />
            <Route path={PATH.LOGIN} render={() => <LoginContainer />} />
            <Route path={PATH.DATAEDITOR} render = {()=> <DataEditorContainer/>}/>
          </main>
          <Footersite />
        </div>
      </BrowserRouter>)
  }
}




export default App;

