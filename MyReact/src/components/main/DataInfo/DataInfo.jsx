import { Preloader } from "../../common/Preloader";

let DataInfo = (props) => {
    return (
        <div>
            <h1>Information</h1>
            {props.props.isFetching? <Preloader/> :<div>
            <ul>Name:<h2>{props.props.memberInformation.workName}</h2></ul>
            <ul>Old:<h2>{props.props.memberInformation.personCount} y.o</h2></ul>
            <ul>Avarage salary:<h2>{props.props.memberInformation.avarageSalary} $</h2></ul>
            </div>}
        </div>

    )
}
export default DataInfo;