import React from "react";
import { connect} from "react-redux";
import { withRouter } from "react-router";
import DataInfo from "./DataInfo.jsx";
import {getInformationById} from "./../../../store/DatabaseReducer"
import {Redirect} from 'react-router-dom'
import { withAuthHocContainer } from "../../common/withAuthHocContainer.js";
import { compose } from "redux";


export class DataInfoContainer extends React.Component 
{
    componentDidMount()
    {
        this.props.getInformationById(this.props.match.params.memberId);
    }

    render()
    {
        if(!this.props.isAutorized) return <Redirect to ='/login'/>
        return(
            <DataInfo props = {this.props}/>
        )
    }
};

let mapStateToProps = (state) =>
{
    return{
        memberInformation: state.dbr.memberInformation,
        isFetching: state.dbr.isFetching,
        isAutorized: state.auth.isAutorized
    }
}


export default compose(connect(mapStateToProps, {getInformationById}), withRouter, withAuthHocContainer)(DataInfoContainer)