import { Formik, Form, Field } from 'formik';
import {loginFormValidation} from "../../../validation/validation"
import s from "./LoginForm.module.css"

let LoginForm = (props) => {

    return <Formik
        initialValues={{ login: "", password: "" }}
        onSubmit={(values) => {
            props.login(values.login, values.password);
            values.login = "";
            values.password = "";
        }}
    >
        {({ errors, touched}) => (
        <Form>
            <div>
                <Field validate ={loginFormValidation} placeholder="login" name="login" type="text" />
                {errors.login && touched.login && <span className ={s.incorrect}>{errors.login}</span>}
            </div>
            <div>
                <Field validate ={loginFormValidation} placeholder="password" name="password" type="password" />
                {errors.password && touched.password && <span className ={s.incorrect} >{errors.password}</span>}
            </div>
            <div>
                <button type="submit">Login</button>
            </div>
        </Form>
         )}
    </Formik>
//return this.props.isAutorized? <Logged logout = {this.props.logout}/> :  <LoginPage message = {this.props.message} login = {this.props.login}/>
}
export default LoginForm;