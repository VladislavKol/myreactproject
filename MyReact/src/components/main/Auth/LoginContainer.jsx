import { connect } from 'react-redux';
import React from 'react';
import { logout, checkAutorizated, login } from '../../../store/Auth'
import LoginForm from './LoginForm';
import s from './LoginForm.module.css'


class Login extends React.Component {


  componentDidMount() {
    this.props.checkAutorizated()
  }
  render() {
    console.log(this.props.message);
    return (
      <div>
        <div>
          {this.props.isAutorized ? <h1>Logout</h1> : <h1>Login</h1>}
          {this.props.message ? <span className={s.incorrect}>{this.props.message}</span> : null}
          {this.props.isAutorized ?<button onClick={() => { this.props.logout() }}>Logout</button>: <LoginForm login={this.props.login} />}
        </div>
        <div>
        </div>

      </div>
    )
  }
}

let mapStateToProps = (state) => {
  return {
    isAutorized: state.auth.isAutorized,
    login: state.auth.login,
    message: state.auth.message
  }
}

const LoginContainer = connect(mapStateToProps, { logout, login, checkAutorizated })(Login);



export default LoginContainer;