import s from './../LoginForm.module.css'
import LoginForm from './../LoginForm';
let LoginPage = (props) => {
    return (
            <div>
                <h1>Login</h1>
                {props.message ? <span className={s.incorrect}>{props.message}</span> : null}
                <LoginForm login={props.login} />
            </div>
    )
}

export default LoginPage;