import { Formik, Form, Field } from 'formik';
import React from "react";
import s from "./DataEditor.module.css"
import {loginFormValidation} from '../../../validation/validation'

class DeleteUser extends React.Component
{
    render()
    {
        return <Formik
        initialValues={{ id: ""}}
        onSubmit={(values) => {
            this.props.deleteUser(values.id);
            values.id = "";
        }}
    >
        {({ errors, touched}) => (
        <Form>
            <div>
                <Field validate ={loginFormValidation} placeholder="id" name="id" type="text" />
                {errors.id && touched.id && <span className ={s.incorrect}>{errors.id}</span>}
            </div>
            <div>
                <button type="submit">Delete</button>
            </div>
        </Form>)}
    </Formik>

    }
}
export default DeleteUser;