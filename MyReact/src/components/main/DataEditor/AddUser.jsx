import { Formik, Form, Field } from 'formik';
import React from "react";
import s from "./DataEditor.module.css"
import {loginFormValidation} from '../../../validation/validation'

class AddUser extends React.Component
{
    render(){ return <Formik
        initialValues={{ WorkName: "", PersonCount: "", AvarageSalary:"" }}
        onSubmit={(values) => {
            this.props.addUser(values.WorkName, values.PersonCount, values.AvarageSalary);
            values.WorkName = "";
            values.PersonCount = "";
            values.AvarageSalary= "";
        }}
    >
        {({ errors, touched}) => (
        <Form>
            <div>
                <Field validate ={loginFormValidation} placeholder="Name" name="WorkName" type="text" />
                {errors.WorkName && touched.WorkName && <span className ={s.incorrect}>{errors.WorkName}</span>}
            </div>
            <div>
                <Field validate ={loginFormValidation} placeholder="Old" name="PersonCount" type="text" />
                {errors.PersonCount && touched.PersonCount && <span className ={s.incorrect} >{errors.PersonCount}</span>}
            </div>
            <div>
                <Field validate ={loginFormValidation} placeholder="Salary" name="AvarageSalary" type="text" />
                {errors.AvarageSalary && touched.AvarageSalary && <span className ={s.incorrect}>{errors.AvarageSalary}</span>}
            </div>
            <div>
                <button type="submit">Add</button>
            </div>
        </Form>
         )}
    </Formik>
    }
   
}
export default AddUser;