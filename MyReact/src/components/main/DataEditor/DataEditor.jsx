import React from "react";
import { API } from "../../../API/api";
import DeleteUser from "./DeleteUser";
import AddUser from "./AddUser";
import { Preloader } from "../../common/Preloader";


class DataEditor extends React.Component {

    render() {
        return <div>
            <h1>DataEditor</h1>
            {this.props.isFetching?<Preloader/> : <div><DeleteUser deleteUser={this.props.deleteUser} />
            <hr></hr>
            <AddUser addUser={this.props.addUser} /> </div>}
        </div>
    }
}
export default DataEditor;