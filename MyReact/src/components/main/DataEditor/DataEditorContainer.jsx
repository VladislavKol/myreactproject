import { connect } from "react-redux";
import { compose } from "redux";
import { withAuthHocContainer } from "../../common/withAuthHocContainer";
import DataEditor  from "./DataEditor";
import {addUser, deleteUser} from '../../../store/DatabaseReducer'


let mapStateToProps = (state) => {
    return{
        isAutorized : state.auth.isAutorized,
        isFetching :state.dbr.isFetching
      }
  }

export default compose(connect(mapStateToProps,{addUser,deleteUser}),withAuthHocContainer)(DataEditor);