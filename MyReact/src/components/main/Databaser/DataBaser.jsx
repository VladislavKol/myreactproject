import React from 'react';
import { NavLink } from 'react-router-dom';
import { Preloader } from '../../common/Preloader';
import ds from './DataBaser.module.css';

class DataBaser extends React.Component {
  textRef = React.createRef();

  componentDidMount() {
    this.selectPage(this.props.currentPage);
  }

  selectPage = (pageNumber) => {
    this.props.selectPage(pageNumber);
  }

  createButton = (pageNumber) => {
    return (<span>
      <button className = {this.props.currentPage === pageNumber && ds.selected} onClick={() => { this.selectPage(pageNumber) }}>
        {pageNumber}
      </button>
    </span>);
  }

  render() {
    console.log(this.props)
    let pages;
    let pagesArray = [];
    if (this.props.pageCount > 0) {
      pages = Math.ceil(this.props.pageCount / 4);
      for (let page = 1; page <= pages; page++) {
        pagesArray.push(page);
      }
    }

    if (this.props.isFetching)
      return (
        <div>
          <h1>DataBaser</h1>
          <Preloader/>
        </div>
      )

    return (
      <div>
        <h1>DataBaser</h1>
        <div>{this.props.worksOnCurrentPage?.map(x => <NavLink activeClassName={ds.active} to={'datainfo/' + x.id}><p id={x.id}>{x.workName}</p></NavLink>)}</div>
        <div>{pagesArray?.map(x => this.createButton(x))}</div>
      </div>
    );
  }

}

export default DataBaser;