import { connect } from 'react-redux';
import DataBaser from './DataBaser.jsx'
import {selectPage} from './../../../store/DatabaseReducer'
import {withAuthHocContainer}  from '../../common/withAuthHocContainer.js';
import { compose } from 'redux';

let mapStateToProps = (state) => {
  return {
    pageCount: state.dbr.pageCount,
    worksOnCurrentPage: state.dbr.worksOnCurrentPage,
    isFetching: state.dbr.isFetching,
    currentPage: state.dbr.currentPage,
    isAutorized: state.auth.isAutorized
  }
}

export default compose(connect(mapStateToProps, {selectPage}),withAuthHocContainer)(DataBaser);