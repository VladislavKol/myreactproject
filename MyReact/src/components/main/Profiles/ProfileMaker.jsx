import React from 'react';
import po from "./ProfileMaker.module.css"

const ProfileMaker = (props) => {
    let profiles = props.profileState.map(x => (
        <div>
            <p>{x.Name}</p>
            <img className={po.imgt} src={x.avatar} />
        </div >))
    return (profiles);
};
export default ProfileMaker;

