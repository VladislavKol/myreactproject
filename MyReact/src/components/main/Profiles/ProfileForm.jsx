import { Field, Form, Formik } from "formik";
import React from "react";

class ProfileForm extends React.Component {
    render() {
        return <div>
            <Formik
                initialValues={{ url: '', profileName: '' }}
                onSubmit={(values) => {
                    this.props.addProfile(values.profileName, values.url)
                    values.url = "";
                    values.profileName = ""
                }}
            >
                <Form>
                    <div>
                        <Field name="url" type='text' placeholder='Url'></Field>
                    </div>
                    <div>
                        <Field name="profileName" type='text' placeholder='Name'></Field>
                    </div>
                    <div>
                        <button type="submit">Add</button>
                    </div>
                </Form>
            </Formik>
        </div>

    }

}
export default ProfileForm;

