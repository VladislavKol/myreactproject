import Profile from './Profile.jsx';
import { connect } from 'react-redux';
import { addNewProfile} from './../../../store/ProfileReducer';
import { withAuthHocContainer } from '../../common/withAuthHocContainer.js';
import { compose } from 'redux';
import { createSelector } from 'reselect';

const getProfileState =(state) =>
{
  return state.state.profile;
}

const getProfileStateSelector = createSelector(getProfileState, (profileState) =>
{
  return profileState;
})

const getAuthorize =(state) =>
{
  return state.auth.isAutorized;
}

const getAuthorizeSelector = createSelector(getAuthorize, (isAuthorized) =>
{
  return isAuthorized;
})



let mapStateToProps = (state) => {
  return{
      profileState: getProfileStateSelector(state),
      isAutorized : getAuthorizeSelector(state) 
    }
  
}

export default compose(connect(mapStateToProps, {addNewProfile}),withAuthHocContainer)(Profile);