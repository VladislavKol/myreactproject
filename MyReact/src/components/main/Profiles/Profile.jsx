import React from 'react';
import ProfileMaker from './ProfileMaker.jsx';
import p from './Profile.module.css'
import { ProfileStatus } from './ProfileStatus.jsx';
import ProfileForm from './ProfileForm.jsx';

class Profile extends React.Component {
  render() {
    console.log("render")
    return (
      <div>
        <h1>Test</h1>
        <ProfileMaker profileState={this.props.profileState} />
        <ProfileForm  addProfile ={this.props.addNewProfile}/>
        <ProfileStatus/>
      </div>
    );
  }
}

export default Profile;