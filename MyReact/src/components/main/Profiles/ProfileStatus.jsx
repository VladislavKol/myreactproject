import React, { useState } from "react"
import { API } from "../../../API/api"
export class ProfileStatus extends React.Component {    
    state = {
        isEditStatus: false,
        status:"_____"
    }
    
    inputRef = React.createRef();

    componentDidMount()
    {
        API.getStatus().then(res => this.setState({status:res}));
    }

    editTrue= ()=> 
    {
        this.setState({isEditStatus:true})
    }

    editFalse=() => 
    {
        this.setState({isEditStatus:false});
        API.updateStatus(this.state.status);
    }

    onChange =() =>
    {
        this.setState({isEditStatus:true});
        this.setState({status:this.inputRef.current.value})
    }

    render() {

        return (
            
            this.state.isEditStatus? <div><span><p>status</p></span><input value = {this.state.status} ref ={this.inputRef} autoFocus = {true} onBlur ={this.editFalse} onChange ={this.onChange}/></div>
            : <div><span><p>status</p></span><span onDoubleClick={this.editTrue}>{this.state.status}</span></div>
        )

    }
}