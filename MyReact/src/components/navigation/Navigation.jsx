import React from 'react';
import n from './Navigation.module.css'
import { NavLink } from 'react-router-dom';


const Navigation = () => {
    //<div><NavLink activeClassName={n.active} to='/profile'><p className ={n.p}>Profile</p></NavLink></div>
    return <nav className={n.navigation}>
        <div><NavLink activeClassName={n.active} to='/databaser'><p>DataBaser</p></NavLink></div>
        <div><NavLink activeClassName={n.active} to='/dataeditor'><p>DataEditor</p></NavLink></div>
        <div><NavLink activeClassName={n.active} to='/profile'><p className ={n.p}>Test</p></NavLink></div>
    </nav>;
}

export default Navigation;