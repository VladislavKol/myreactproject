import React from "react"
import { Redirect } from "react-router-dom"



export let withAuthHocContainer = (Component) => {
    class withAuth extends React.Component {
        render() {
            if(!this.props.isAutorized) return <Redirect to='/login' />
            return <Component {...this.props} />
        }
    }
    return withAuth;
}

