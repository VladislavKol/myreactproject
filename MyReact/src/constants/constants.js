export const ACTIONS = {
  ADD_NAME : "ADD_NAME",
  ADD_REF : "ADD_REF",
  ADD_PROFILE : "ADD_PROFILE",

  };

  export const PATH = {
    PROFILE: '/profile',
    DATABASER: '/databaser',
    DATAINFO: '/datainfo/:memberId',
    LOGIN: '/login',
    DATAEDITOR: '/dataeditor'
  };

  export const DATA_ACTIONS = 
  {
    TAKE_ALL_PAGES: "TAKE_ALL_PAGES",
    TAKE_INFORMATION_BY_ID: "TAKE_INFORMATION_BY_ID",
    FETCH_RUN: "FETCH_RUN",
    FETCH_COMPLETE : "FETCH_COMPLETE",
    DELETE_BY_ID:"DELETE_BY_ID", 
    ADD_USER:"ADD_USER"
  }

  export const AUTH_ACTIONS = 
  {
    GET_STATUS:"GET_STATUS",
    LOG_IN :"LOG_IN",
    LOG_OUT:"LOGOUT" ,
  }

