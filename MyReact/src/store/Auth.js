import { AUTH_ACTIONS } from "../constants/constants";
import { API } from "../API/api";


const initialState =
{
    isAutorized: false,
    login: null,
    message: null
};


export const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_ACTIONS.GET_STATUS:
            return { ...state, isAutorized: action.isAutorized }
        case AUTH_ACTIONS.LOG_IN:
            return { ...state, login: action.login, message: action.message }
        case AUTH_ACTIONS.LOG_OUT:
            return { ...state, login: null, password: null, isAutorized: false }
        default:
            return state;
    }
}


//Action creators
export const checkAutorization = (isAutorized) => ({ type: AUTH_ACTIONS.GET_STATUS, isAutorized });
export const autorize = (login, message) => ({ type: AUTH_ACTIONS.LOG_IN, login, message });
export const logoutFromSystem = () => ({ type: AUTH_ACTIONS.LOG_OUT });


//Thunks
export const checkAutorizated = () => (dispatch) => { API.authme().then(res => { dispatch(checkAutorization(res)) }) }


export const logout = () => (dispatch) => { API.logout(); dispatch(logoutFromSystem());}


export const login = (login, password) => {
    return (dispatch) => {
        API.login(login, password)
            .then(res => {
                dispatch(autorize(res.login, res.message));
                let a;
                if (res.message == null) a = true;
                dispatch(checkAutorization(a));
            }
            )
    }
}


