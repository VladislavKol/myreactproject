import {applyMiddleware, combineReducers, createStore} from "redux";
import { DatabaseReducer } from "./DatabaseReducer";
import {ProfileReducer} from "./ProfileReducer";
import{AuthReducer} from "./Auth"
import thunk from "redux-thunk";

let reducers = combineReducers({
state : ProfileReducer,
dbr : DatabaseReducer,
auth: AuthReducer
});

let store = createStore(reducers, applyMiddleware(thunk));

export default store;