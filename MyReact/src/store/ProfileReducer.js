import { ACTIONS } from "../constants/constants";


const initialState =
{
    profile: []
};


export const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTIONS.ADD_PROFILE:
            console.log(state.profile)
            return { ...state, profile: [...state.profile, { Name: action.newName, avatar: action.newUrl }] }
        default:
            return state;
    }
}


//Action creators
export const addProfile = (newName, newUrl) => ({ type: ACTIONS.ADD_PROFILE,newName, newUrl });


//Thunks
export const addNewProfile = (newName, newUrl) => {
    return (dispatch) => {
        dispatch(addProfile(newName, newUrl));

    }
}
