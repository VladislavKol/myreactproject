import { DATA_ACTIONS } from "../constants/constants";
import { API } from "../API/api";


const initialState =
{
    worksOnCurrentPage: [],
    pageCount: 0,
    isFetching: false,
    currentPage: 1,
    memberInformation: { id: 0, workName: '', personCount: 0, avarageSalary: 0 }
};


export const DatabaseReducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_ACTIONS.TAKE_ALL_PAGES:
            return { ...state, worksOnCurrentPage: [...action.worksOnCurrentPage], pageCount: action.amountWorksOnPage, currentPage: action.currentPage }
        case DATA_ACTIONS.FETCH_RUN:
            return { ...state, isFetching: true }
        case DATA_ACTIONS.FETCH_COMPLETE:
            return { ...state, isFetching: false }
        case DATA_ACTIONS.TAKE_INFORMATION_BY_ID:
            return { ...state, memberInformation: action.memberInformation }
        default:
            return state;
    }
}


//Action creators
export const setCurrentPage = (worksOnCurrentPage, amountWorksOnPage, currentPage) => ({ type: DATA_ACTIONS.TAKE_ALL_PAGES, worksOnCurrentPage, amountWorksOnPage, currentPage });
export const setInformation = (memberInformation) => ({ type: DATA_ACTIONS.TAKE_INFORMATION_BY_ID, memberInformation });
export const fetchRun = () => ({ type: DATA_ACTIONS.FETCH_RUN });
export const fetchComplete = () => ({ type: DATA_ACTIONS.FETCH_COMPLETE });


//Thunks
export const selectPage = (currentPage) => {
    return (dispatch) => {
        dispatch(fetchRun());
        API.getPage(currentPage).then((data) => {
            dispatch(fetchComplete());
            dispatch(setCurrentPage(data.workInfo, data.worksCount, currentPage));
        });
    }
}

export const addUser = (name, yo,salary) => {
    return (dispatch) => {
        dispatch(fetchRun());
        API.addUser(name, yo, salary).then(() => {
            dispatch(fetchComplete());
        });
    }
}
export const deleteUser = (id) => {
    return (dispatch) => {
        dispatch(fetchRun());
        API.deleteUser(id).then(() => {
            dispatch(fetchComplete());
        });
    }
}

export const getInformationById = (memberId) => {
    return (dispatch) => {
        dispatch(fetchRun());
        API.getMemberInfo(memberId)
            .then(res => {
                dispatch(fetchComplete());
                dispatch(setInformation(res));})

    }
}

