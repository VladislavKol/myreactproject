import axios from "axios";

const instance = axios.create({
    baseURL: 'https://localhost:44388',
    withCredentials: true,
}
);

export const API = {

    getPage(currentPage)
    {
        return instance.get('work?page=' + currentPage + '&pageSize=4').then(res => {return res.data});
    },

    getMemberInfo(memberId)
    {
        return instance.get("name?id=" + memberId).then(res =>{return res.data});
    },

    authme()
    {
        return instance.get('/authme').then((res) => {return res.data});
    },

    login(inputLogin,inputPassword)
    {
        return instance.post('/login', {Login:inputLogin, Password:inputPassword }).then(res => { return res.data});
    },
    logout()
    {
        return instance.post('/logout').then(res => {return res.data});
    },
    deleteUser(userId)
    {
        return instance.delete('/deleteUser?id='+ userId).then(res => {return res.data});
    },
    addUser(name, count, salary)
    {
        return instance.post('/adduser', {id:2,WorkName:name, PersonCount:parseInt(count), AvarageSalary:parseInt(salary)});
    },
    getStatus()
    {
        return instance.get('/current').then(res => {return res.data})
    },
    updateStatus(text)
    {
        return instance.post('/update', {id :2 ,Stattus:text});
    }
}


